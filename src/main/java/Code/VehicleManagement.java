package Code;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class VehicleManagement {
    private VehicleManagement() {
        getDBVechicle();
    }

    public static VehicleManagement vm = new VehicleManagement();

    public static VehicleManagement getInstance() {
        return vm;
    }

    private List<Vehicle> list = new ArrayList<Vehicle>();

    public List<Vehicle> getList() {
        return list;
    }

    private void getDBVechicle() {
        Connection conn = GetConnection.connection();
        String sql = "select * from vehicle";
        try {
            PreparedStatement pst = (PreparedStatement) conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            int user_id;
            int vehicle_id;
            int fahrzeugkomponente_id;
            int elektromoter_id;
            int energiespeicher_id;
            String mark;
            while (rs.next()) {
                vehicle_id = rs.getInt(1);
                user_id = rs.getInt(2);
                fahrzeugkomponente_id = rs.getInt(3);
                elektromoter_id = rs.getInt(4);
                energiespeicher_id = rs.getInt(5);
                mark = rs.getString(6);
                Vehicle v = new Vehicle(vehicle_id, fahrzeugkomponente_id, elektromoter_id, energiespeicher_id, mark,user_id);
                this.list.add(v);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public boolean addVehicle(int user_id,int fahrzeugkomponente_id,int elektromotor_id, int energiespeicher_id, String marke) {
        Connection conn = GetConnection.connection();
        String sql = "insert into vehicle(fahrzeugkomponente_id,elektromotor_id,energiespeicher_id,marke,user_id) values(?,?,?,?,?)";
        PreparedStatement pstmt;
        try {
            pstmt = (PreparedStatement) conn.prepareStatement(sql);
            pstmt.setInt(1, fahrzeugkomponente_id);
            pstmt.setInt(2, elektromotor_id);
            pstmt.setInt(3, energiespeicher_id);
            pstmt.setString(4, marke);
            pstmt.setInt(5,user_id);
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
            System.out.printf("insert success");

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        getDBVechicle();
        return true;
    }



}

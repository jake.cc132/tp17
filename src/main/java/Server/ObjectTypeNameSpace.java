package Server;

import org.eclipse.milo.opcua.sdk.core.AccessLevel;
import org.eclipse.milo.opcua.sdk.core.Reference;
import org.eclipse.milo.opcua.sdk.core.ValueRank;
import org.eclipse.milo.opcua.sdk.server.OpcUaServer;
import org.eclipse.milo.opcua.sdk.server.api.DataItem;
import org.eclipse.milo.opcua.sdk.server.api.ManagedNamespace;
import org.eclipse.milo.opcua.sdk.server.api.MonitoredItem;
import org.eclipse.milo.opcua.sdk.server.model.nodes.objects.BaseEventNode;
import org.eclipse.milo.opcua.sdk.server.model.nodes.objects.ServerNode;
import org.eclipse.milo.opcua.sdk.server.nodes.*;
import org.eclipse.milo.opcua.sdk.server.util.SubscriptionModel;
import org.eclipse.milo.opcua.stack.core.Identifiers;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.types.builtin.*;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.crypto.Data;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.Unsigned.*;

public class ObjectTypeNameSpace extends ManagedNamespace {


    public static final String URI = "urn:my:custom:namespace";

    private final SubscriptionModel subscriptionModel;
    private Logger logger = LoggerFactory.getLogger(getClass());

    public ObjectTypeNameSpace(OpcUaServer server, String namespaceUri) {
        super(server, namespaceUri);
        this.subscriptionModel = new SubscriptionModel(server, this);
        onStartup();
    }

    final UaFolderNode VEHICLES = new UaFolderNode(
            getNodeContext(),
            newNodeId("Vehicles"),
            newQualifiedName("Vehicles"),
            LocalizedText.english("Vehicles"));

    public UaFolderNode getVEHICLES() {
        return VEHICLES;
    }


    protected void onStartup() {
        super.onStartup();


        getNodeManager().addNode(VEHICLES);


        VEHICLES.addReference(new Reference(
                VEHICLES.getNodeId(),
                Identifiers.Organizes,
                Identifiers.ObjectsFolder.expanded(), false
        ));


        //users folder
        final UaFolderNode USERS = new UaFolderNode(
                getNodeContext(),
                newNodeId("Users"),
                newQualifiedName("Users"),
                LocalizedText.english("Users"));


        USERS.addReference(new Reference(
                USERS.getNodeId(),
                Identifiers.Organizes,
                Identifiers.ObjectsFolder.expanded(), false
        ));
        getNodeContext().getNodeManager().addNode(USERS);


        //recyclernode folder
        final UaFolderNode recyclerNode = new UaFolderNode(
                getNodeContext(),
                newNodeId("Recycler"),
                newQualifiedName("Recycler"),
                LocalizedText.english("Recycler"));

        getNodeContext().getNodeManager().addNode(recyclerNode);

        recyclerNode.addReference(new Reference(
                recyclerNode.getNodeId(),
                Identifiers.Organizes,
                USERS.getNodeId().expanded(), false
        ));

        USERS.addOrganizes(recyclerNode);

        //owner folder
        final UaFolderNode ownerNode = new UaFolderNode(
                getNodeContext(),
                newNodeId("Owner"),
                newQualifiedName("Owner"),
                LocalizedText.english("Owner"));

        getNodeContext().getNodeManager().addNode(ownerNode);


        ownerNode.addReference(new Reference(
                ownerNode.getNodeId(),
                Identifiers.Organizes,
                USERS.getNodeId().expanded(), false
        ));


        USERS.addOrganizes(ownerNode);

        UaObjectTypeNode vehicleType = addVehicleType();
        addVehiclesObject(VEHICLES, vehicleType);
        addObject2(recyclerNode);
        addObject3(ownerNode);



        UaNode serverNode = getServer()
                .getAddressSpaceManager()
                .getManagedNode(Identifiers.Server)
                .orElse(null);

        if (serverNode instanceof ServerNode) {
            ((ServerNode) serverNode).setEventNotifier(ubyte(1));

            // Post a bogus Event every couple seconds
            getServer().getScheduledExecutorService().scheduleAtFixedRate(() -> {
                try {
                    BaseEventNode eventNode = getServer().getEventFactory().createEvent(
                            newNodeId(UUID.randomUUID()),
                            Identifiers.BaseEventType
                    );

                    eventNode.setBrowseName(new QualifiedName(1, "foo"));
                    eventNode.setDisplayName(LocalizedText.english("foo"));
                    eventNode.setEventId(ByteString.of(new byte[]{0, 1, 2, 3}));
                    eventNode.setEventType(Identifiers.BaseEventType);
                    eventNode.setSourceNode(serverNode.getNodeId());
                    eventNode.setSourceName(serverNode.getDisplayName().getText());
                    eventNode.setTime(DateTime.now());
                    eventNode.setReceiveTime(DateTime.NULL_VALUE);
                    eventNode.setMessage(LocalizedText.english("event message!"));
                    eventNode.setSeverity(ushort(2));

                    getServer().getEventBus().post(eventNode);

                    eventNode.delete();
                } catch (Throwable e) {
                    logger.error("Error creating EventNode: {}", e.getMessage(), e);
                }
            }, 0, 2, TimeUnit.SECONDS);
        }
    }


    private UaObjectTypeNode addVehicleType() {
        UaObjectTypeNode vehicleType = UaObjectTypeNode.builder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Vehicle"))
                .setBrowseName(newQualifiedName("Vehicle"))
                .setDisplayName(LocalizedText.english("Vehicle"))
                .setIsAbstract(false)
                .build();

        //VariableNode Vehicle ID und Reference
        UaVariableNode Vehicle_id = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Vehicle/Vehicle_id"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("Vehicle_id"))
                .setDisplayName(LocalizedText.english("Vehicle_id"))
                .setDataType(Identifiers.Integer)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        Vehicle_id.addReference(new Reference(
                Vehicle_id.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        Vehicle_id.setValue(new DataValue(new Variant(2)));

        vehicleType.addComponent(Vehicle_id);
        getNodeManager().addNode(Vehicle_id);

        //VariableNode Vehicle ID und Reference
        UaVariableNode Vehicle_Credit = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Vehicle/Vehicle_Credit"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("Vehicle_Credit"))
                .setDisplayName(LocalizedText.english("Vehicle_Credit"))
                .setDataType(Identifiers.Int32)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        Vehicle_Credit.addReference(new Reference(
                Vehicle_Credit.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        Vehicle_Credit.setValue(new DataValue(new Variant(30)));

        vehicleType.addComponent(Vehicle_Credit);
        getNodeManager().addNode(Vehicle_Credit);

        addEnergiespeicherVehicle(vehicleType);
        addElektromotorVehicle(vehicleType);
        addFahrzeugkomponentVehicle(vehicleType);
        addOwnerIdVehicle(vehicleType);

        getServer().getObjectTypeManager().registerObjectType(
                vehicleType.getNodeId(),
                UaObjectNode.class,
                UaObjectNode::new
        );


        vehicleType.addReference(new Reference(
                vehicleType.getNodeId(),
                Identifiers.HasSubtype,
                Identifiers.BaseObjectType.expanded(),
                false
        ));

        getNodeManager().addNode(vehicleType);
        return vehicleType;

    }

    private void addVehiclesObject(UaFolderNode rootNode, UaObjectTypeNode vehicleType) {
        try {

            UaObjectNode vehicle = (UaObjectNode) getNodeFactory().createNode(
                    newNodeId("Vehicles/vehicle"),
                    vehicleType.getNodeId(),
                    false
            );

            rootNode.addOrganizes(vehicle);

        } catch (UaException e) {
            logger.error("Error creating MyObjectType instance: {}", e.getMessage(), e);
        }
    }


    private void addEnergiespeicherVehicle(UaObjectTypeNode vehicleType) {
        //ObjectNode Energiespeicher
        UaObjectTypeNode Energiespeicher = UaObjectTypeNode.builder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Energiespeicher"))
                .setBrowseName(newQualifiedName("Energiespeicher"))
                .setDisplayName(LocalizedText.english("Energiespeicher"))
                .setIsAbstract(false)
                .build();

        //VariableNode Energiespeicher ID und Reference
        UaVariableNode Energiespeicher_id = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Energiespeicher/Energiespeicher_id"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("Energiespeicher_id"))
                .setDisplayName(LocalizedText.english("Energiespeicher_id"))
                .setDataType(Identifiers.Integer)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        Energiespeicher_id.addReference(new Reference(
                Energiespeicher_id.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        //VariableNode Energiespeicher Name und Reference
        UaVariableNode Energiespeicher_name = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Energiespeicher/Energiespericher_name"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("Energiespericher_name"))
                .setDisplayName(LocalizedText.english("Energiespericher_name"))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        Energiespeicher_name.addReference(new Reference(
                Energiespeicher_name.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));


        //VariableNode Energiespeicher Produktion Jahr und Reference
        UaVariableNode Energiespeicher_produ_jahr = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Energiespeicher/Energiespeicer_jahr"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("Energiespeicer_jahr"))
                .setDisplayName(LocalizedText.english("Energiespeicer_jahr"))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        Energiespeicher_produ_jahr.addReference(new Reference(
                Energiespeicher_produ_jahr.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        //VariableNode Energiespeicher Health und Reference
        UaVariableNode Energiespeicher_health = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Energiespeicher/Energiespeicher_health"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("Energiespeicher_health"))
                .setDisplayName(LocalizedText.english("Energiespeicher_health"))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        Energiespeicher_health.addReference(new Reference(
                Energiespeicher_health.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));


        //VariableNode Energiespeicher Credit und Reference
        UaVariableNode Energiespeicher_credit = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Energiespeicher/Energiespeicher_credit"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("Energiespeicher_credit"))
                .setDisplayName(LocalizedText.english("Energiespeicher_credit"))
                .setDataType(Identifiers.Integer)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        Energiespeicher_credit.addReference(new Reference(
                Energiespeicher_credit.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

//        Variable: setzen Wert und addComponent
        Energiespeicher_id.setValue(new DataValue(new Variant(0)));
        Energiespeicher_name.setValue(new DataValue(new Variant("good name")));
        Energiespeicher_produ_jahr.setValue(new DataValue(new Variant("1995")));
        Energiespeicher_health.setValue(new DataValue(new Variant("gut")));
        Energiespeicher_credit.setValue(new DataValue(new Variant(15)));

        Energiespeicher.addComponent(Energiespeicher_id);
        Energiespeicher.addComponent(Energiespeicher_name);
        Energiespeicher.addComponent(Energiespeicher_produ_jahr);
        Energiespeicher.addComponent(Energiespeicher_health);
        Energiespeicher.addComponent(Energiespeicher_credit);

        getNodeManager().addNode(Energiespeicher_id);
        getNodeManager().addNode(Energiespeicher_name);
        getNodeManager().addNode(Energiespeicher_produ_jahr);
        getNodeManager().addNode(Energiespeicher_health);
        getNodeManager().addNode(Energiespeicher_credit);

        //add HochvoltBattrie to energiespeicher
        addHochVoltBatterieEnergiespeicer(Energiespeicher);

        getServer().getObjectTypeManager().registerObjectType(
                Energiespeicher.getNodeId(),
                UaObjectNode.class,
                UaObjectNode::new
        );

        Energiespeicher.addReference(new Reference(
                Energiespeicher.getNodeId(),
                Identifiers.HasSubtype,
                Identifiers.BaseObjectType.expanded(),
                false
        ));

        getNodeManager().addNode(Energiespeicher);


        try {
            UaObjectNode EnergiespeicherObject = (UaObjectNode) getNodeFactory().createNode(
                    newNodeId("Vehicles/Vehicle/Energiespeicher"),
                    Energiespeicher.getNodeId(),
                    false
            );


            EnergiespeicherObject.addReference(new Reference(
                    EnergiespeicherObject.getNodeId(),
                    Identifiers.HasModellingRule,
                    Identifiers.ModellingRule_Mandatory.expanded(),
                    true
            ));


            for (UaNode node : EnergiespeicherObject.getComponentNodes()
            ) {

                node.addReference(new Reference(
                        node.getNodeId(),
                        Identifiers.HasModellingRule,
                        Identifiers.ModellingRule_Mandatory.expanded(),
                        true
                ));

                if (node.getBrowseName().equals(newQualifiedName("HochVoltBatterie"))) {
                    node = (UaObjectNode) node;
                    for (UaNode nd : ((UaObjectNode) node).getComponentNodes()
                    ) {
                        nd.addReference(new Reference(
                                nd.getNodeId(),
                                Identifiers.HasModellingRule,
                                Identifiers.ModellingRule_Mandatory.expanded(),
                                true
                        ));

                    }
                }

            }
            EnergiespeicherObject.setBrowseName(newQualifiedName("Energiespeicher"));
            EnergiespeicherObject.setDisplayName(LocalizedText.english("Energiespeicher"));
            getNodeManager().addNode(EnergiespeicherObject);

            vehicleType.addComponent(EnergiespeicherObject);


        } catch (UaException e) {
            logger.error("Error creating MyObjectType instance: {}", e.getMessage(), e);
        }


    }

    private void addElektromotorVehicle(UaObjectTypeNode vehicleType) {

        //ObjectNode Elektromotor
        UaObjectTypeNode Elektromotor = UaObjectTypeNode.builder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Elektromotor"))
                .setBrowseName(newQualifiedName("Elektromotor"))
                .setDisplayName(LocalizedText.english("Elektromotor"))
                .setIsAbstract(false)
                .build();

        //VariableNode Elektromotor ID
        UaVariableNode Elektromotor_id = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Elektromotor/Elektromotor_id"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("Elektromotor_id"))
                .setDisplayName(LocalizedText.english("Elektromotor_id"))
                .setDataType(Identifiers.Integer)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        Elektromotor_id.addReference(new Reference(
                Elektromotor_id.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        //VariableNode Elektromotor Name und Reference
        UaVariableNode Elektromotor_name = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Elektromotor/Elektromotor_name"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("Elektromotor_name"))
                .setDisplayName(LocalizedText.english("Elektromotor_name"))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        Elektromotor_name.addReference(new Reference(
                Elektromotor_name.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        //VariableNode Elektromotor Credit und Reference
        UaVariableNode Elektromotor_credit = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Elektromotor/Elektromotor_credit"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("Elektromotor_credit"))
                .setDisplayName(LocalizedText.english("Elektromotor_credit"))
                .setDataType(Identifiers.Integer)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        Elektromotor_credit.addReference(new Reference(
                Elektromotor_credit.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        //Variable: setzen Wert und addComponent
        Elektromotor_id.setValue(new DataValue(new Variant(1)));
        Elektromotor_name.setValue(new DataValue(new Variant("elektromotor")));
        Elektromotor_credit.setValue(new DataValue(new Variant(20)));

        Elektromotor.addComponent(Elektromotor_id);
        Elektromotor.addComponent(Elektromotor_name);
        Elektromotor.addComponent(Elektromotor_credit);

        getNodeManager().addNode(Elektromotor_id);
        getNodeManager().addNode(Elektromotor_name);
        getNodeManager().addNode(Elektromotor_credit);

        getServer().getObjectTypeManager().registerObjectType(
                Elektromotor.getNodeId(),
                UaObjectNode.class,
                UaObjectNode::new
        );

        Elektromotor.addReference(new Reference(
                Elektromotor.getNodeId(),
                Identifiers.HasSubtype,
                Identifiers.BaseObjectType.expanded(),
                false
        ));
        getNodeManager().addNode(Elektromotor);

        try {
            // new ElektromotorObject
            UaObjectNode ElektromotorObject = (UaObjectNode) getNodeFactory().createNode(
                    newNodeId("Vehicles/Vehicle/Elektromotor"),
                    Elektromotor.getNodeId(),
                    false
            );

            ElektromotorObject.addReference(new Reference(
                    ElektromotorObject.getNodeId(),
                    Identifiers.HasModellingRule,
                    Identifiers.ModellingRule_Mandatory.expanded(),
                    true
            ));

            for (UaNode node : ElektromotorObject.getComponentNodes()
            ) {
                node.addReference(new Reference(
                        node.getNodeId(),
                        Identifiers.HasModellingRule,
                        Identifiers.ModellingRule_Mandatory.expanded(),
                        true
                ));
            }

            ElektromotorObject.setBrowseName(newQualifiedName("Elektromotor"));
            ElektromotorObject.setDisplayName(LocalizedText.english("Elektromotor"));
            getNodeManager().addNode(ElektromotorObject);

            vehicleType.addComponent(ElektromotorObject);
        } catch (UaException e) {
            logger.error("Error creating MyObjectType instance: {}", e.getMessage(), e);
        }
    }

    private void addFahrzeugkomponentVehicle(UaObjectTypeNode vehicleType) {
        UaObjectTypeNode Fahrzeugkomponent = UaObjectTypeNode.builder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Fahrzeugkomponent"))
                .setBrowseName(newQualifiedName("Fahrzeugkomponent"))
                .setDisplayName(LocalizedText.english("Fahrzeugkomponent"))
                .setIsAbstract(false)
                .build();

        //VariableNode Fahrzeugkomponent ID und Reference
        UaVariableNode Fahrzeugkomponent_id = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Fahrzeugkomponent/Fahrzeugkomponent_id"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("Fahrzeugkomponent_id"))
                .setDisplayName(LocalizedText.english("Fahrzeugkomponent_id"))
                .setDataType(Identifiers.Integer)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        Fahrzeugkomponent_id.addReference(new Reference(
                Fahrzeugkomponent_id.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));


        //VariableNode Fahrzeugkomponent Name und Reference
        UaVariableNode Fahrzeugkomponent_name = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Fahrzeugkomponent/Fahrzeugkomponent_name"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("Fahrzeugkomponent_name"))
                .setDisplayName(LocalizedText.english("Fahrzeugkomponent_name"))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        Fahrzeugkomponent_name.addReference(new Reference(
                Fahrzeugkomponent_name.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        //VariableNode Fahrzeugkomponent Credit und Reference
        UaVariableNode Fahrzeugkomponent_credit = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Fahrzeugkomponent/Fahrzeugkomponent_credit"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("Fahrzeugkomponent_credit"))
                .setDisplayName(LocalizedText.english("Fahrzeugkomponent_credit"))
                .setDataType(Identifiers.Integer)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        Fahrzeugkomponent_credit.addReference(new Reference(
                Fahrzeugkomponent_credit.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));
//        //Variable: setzen Wert und addComponent
        Fahrzeugkomponent_id.setValue(new DataValue(new Variant(4)));
        Fahrzeugkomponent_name.setValue(new DataValue(new Variant("Fahrzeugkomponent")));
        Fahrzeugkomponent_credit.setValue(new DataValue(new Variant(10)));

        Fahrzeugkomponent.addComponent(Fahrzeugkomponent_id);
        Fahrzeugkomponent.addComponent(Fahrzeugkomponent_name);
        Fahrzeugkomponent.addComponent(Fahrzeugkomponent_credit);

        getNodeManager().addNode(Fahrzeugkomponent_id);
        getNodeManager().addNode(Fahrzeugkomponent_name);
        getNodeManager().addNode(Fahrzeugkomponent_credit);

        getServer().getObjectTypeManager().registerObjectType(
                Fahrzeugkomponent.getNodeId(),
                UaObjectNode.class,
                UaObjectNode::new
        );

        Fahrzeugkomponent.addReference(new Reference(
                Fahrzeugkomponent.getNodeId(),
                Identifiers.HasSubtype,
                Identifiers.BaseObjectType.expanded(),
                false
        ));
        getNodeManager().addNode(Fahrzeugkomponent);

        try {
            // new FahrzeugkomponentObject
            UaObjectNode FahrzeugkomponentObject = (UaObjectNode) getNodeFactory().createNode(
                    newNodeId("Vehicles/Vehicle/Fahrzeugkomponent"),
                    Fahrzeugkomponent.getNodeId(),
                    false
            );

            FahrzeugkomponentObject.addReference(new Reference(
                    FahrzeugkomponentObject.getNodeId(),
                    Identifiers.HasModellingRule,
                    Identifiers.ModellingRule_Mandatory.expanded(),
                    true
            ));

            for (UaNode node : FahrzeugkomponentObject.getComponentNodes()
            ) {
                node.addReference(new Reference(
                        node.getNodeId(),
                        Identifiers.HasModellingRule,
                        Identifiers.ModellingRule_Mandatory.expanded(),
                        true
                ));
            }

            FahrzeugkomponentObject.setBrowseName(newQualifiedName("Fahrzeugkomponent"));
            FahrzeugkomponentObject.setDisplayName(LocalizedText.english("Fahrzeugkomponent"));
            getNodeManager().addNode(FahrzeugkomponentObject);

            vehicleType.addComponent(FahrzeugkomponentObject);
        } catch (UaException e) {
            logger.error("Error creating MyObjectType instance: {}", e.getMessage(), e);
        }

    }

    private void addOwnerIdVehicle(UaObjectTypeNode vehicleType){
        UaVariableNode Owner_Id = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("Owner_id_Vehicle"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("Owner_id_Vehicle"))
                .setDisplayName(LocalizedText.english("Owner_id_Vehicle"))
                .setDataType(Identifiers.Int32)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        Owner_Id.addReference(new Reference(
                Owner_Id.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        Owner_Id.setValue(new DataValue(new Variant(0)));
        vehicleType.addComponent(Owner_Id);
        getNodeManager().addNode(Owner_Id);


    }

    private void addHochVoltBatterieEnergiespeicer(UaObjectTypeNode energiespeicher) {
        //ObjectNode HochVoltBatterie
        UaObjectTypeNode HochVoltBatterie = UaObjectTypeNode.builder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/HochVoltBatterie"))
                .setBrowseName(newQualifiedName("HochVoltBatterie"))
                .setDisplayName(LocalizedText.english("HochVoltBatterie"))
                .setIsAbstract(false)
                .build();


        //VariableNode HochVoltBatterie ID und Reference
        UaVariableNode HochVoltBatterie_id = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/HochVoltBatterie/HochVoltBatterie_id"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("HochVoltBatterie_id"))
                .setDisplayName(LocalizedText.english("HochVoltBatterie_id"))
                .setDataType(Identifiers.Integer)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        HochVoltBatterie_id.addReference(new Reference(
                HochVoltBatterie_id.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        //VariableNode HochVoltBatterie Name und Reference
        UaVariableNode HochVoltBatterie_name = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/HochVoltBatterie/HochVoltBatterie_name"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("HochVoltBatterie_name"))
                .setDisplayName(LocalizedText.english("HochVoltBatterie_name"))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        HochVoltBatterie_name.addReference(new Reference(
                HochVoltBatterie_name.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        //VariableNode HochVoltBatterie Credit und Reference
        UaVariableNode HochVoltBatterie_credit = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/HochVoltBatterie/HochVoltBatterie_credit"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("HochVoltBatterie_credit"))
                .setDisplayName(LocalizedText.english("HochVoltBatterie_credit"))
                .setDataType(Identifiers.Integer)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        HochVoltBatterie_credit.addReference(new Reference(
                HochVoltBatterie_credit.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        UaVariableNode StateOfHealthnode = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/HochVoltBatterie/StateOfHealth"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("StateOfHealth"))
                .setDisplayName(LocalizedText.english("StateOfHealth"))
                .setDataType(Identifiers.Integer)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        StateOfHealthnode.addReference(new Reference(
                StateOfHealthnode.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        //Variable Gewicht
        UaVariableNode Gewichtnode = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/HochVoltBatterie/Gewicht"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("Gewicht"))
                .setDisplayName(LocalizedText.english("Gewicht"))
                .setDataType(Identifiers.Integer)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        Gewichtnode.addReference(new Reference(
                Gewichtnode.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        //Variable ProduktionsJahr
        UaVariableNode ProduktionsJahrnode = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/HochVoltBatterie/ProduktionsJahr"))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName("ProduktionsJahr"))
                .setDisplayName(LocalizedText.english("ProduktionsJahr"))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        ProduktionsJahrnode.addReference(new Reference(
                ProduktionsJahrnode.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));


//        //Variable: setzen Wert und addComponent
        HochVoltBatterie_id.setValue(new DataValue(new Variant(8)));
        HochVoltBatterie_name.setValue(new DataValue(new Variant("HochVoltBatterie")));
        HochVoltBatterie_credit.setValue(new DataValue(new Variant(5)));
        StateOfHealthnode.setValue(new DataValue(new Variant(2)));
        Gewichtnode.setValue(new DataValue(new Variant(9)));
        ProduktionsJahrnode.setValue(new DataValue(new Variant("3")));

        HochVoltBatterie.addComponent(HochVoltBatterie_id);
        HochVoltBatterie.addComponent(HochVoltBatterie_name);
        HochVoltBatterie.addComponent(HochVoltBatterie_credit);
        HochVoltBatterie.addComponent(StateOfHealthnode);
        HochVoltBatterie.addComponent(Gewichtnode);
        HochVoltBatterie.addComponent(ProduktionsJahrnode);


        getServer().getObjectTypeManager().registerObjectType(
                HochVoltBatterie.getNodeId(),
                UaObjectNode.class,
                UaObjectNode::new
        );

        HochVoltBatterie.addReference(new Reference(
                HochVoltBatterie.getNodeId(),
                Identifiers.HasSubtype,
                Identifiers.BaseObjectType.expanded(),
                false
        ));

        getNodeManager().addNode(HochVoltBatterie_id);
        getNodeManager().addNode(HochVoltBatterie_name);
        getNodeManager().addNode(HochVoltBatterie_credit);
        getNodeManager().addNode(StateOfHealthnode);
        getNodeManager().addNode(Gewichtnode);
        getNodeManager().addNode(ProduktionsJahrnode);
        getNodeManager().addNode(HochVoltBatterie);

        try {
            UaObjectNode HochVoltBatterieObject = (UaObjectNode) getNodeFactory().createNode(
                    newNodeId("Vehicles/Vehicle/HochVoltBatterie"),
                    HochVoltBatterie.getNodeId(),
                    false
            );

            HochVoltBatterieObject.addReference(new Reference(
                    HochVoltBatterieObject.getNodeId(),
                    Identifiers.HasModellingRule,
                    Identifiers.ModellingRule_Mandatory.expanded(),
                    true
            ));

            for (UaNode node : HochVoltBatterieObject.getComponentNodes()
            ) {
                node.addReference(new Reference(
                        node.getNodeId(),
                        Identifiers.HasModellingRule,
                        Identifiers.ModellingRule_Mandatory.expanded(),
                        true
                ));
            }
            energiespeicher.addComponent(HochVoltBatterieObject);


        } catch (UaException e) {
            logger.error("Error creating MyObjectType instance: {}", e.getMessage(), e);
        }
    }

    private void addObject2(UaFolderNode rootNode) {
        UaObjectTypeNode Recyclertype = UaObjectTypeNode.builder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Recyclertype"))
                .setBrowseName(newQualifiedName("Recyclertype"))
                .setDisplayName(LocalizedText.english("Recyclertype"))
                .setIsAbstract(false)
                .build();
        getNodeManager().addNode(Recyclertype);
        getServer().getObjectTypeManager().registerObjectType(
                Recyclertype.getNodeId(),
                UaObjectNode.class,
                UaObjectNode::new
        );

        Recyclertype.addReference(new Reference(
                Recyclertype.getNodeId(),
                Identifiers.HasSubtype,
                Identifiers.BaseObjectType.expanded(),
                false
        ));

        String name = "User_ID";
        UaVariableNode node1 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name))
                .setDisplayName(LocalizedText.english(name))
                .setDataType(Identifiers.Int32)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        node1.addReference(new Reference(
                node1.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        node1.setValue(new DataValue(new Variant(1)));
        String name2 = "User_Name";
        UaVariableNode node2 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name2))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name2))
                .setDisplayName(LocalizedText.english(name2))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        node2.setValue(new DataValue(new Variant(0)));
        node2.addReference(new Reference(
                node2.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));
        node2.setValue(new DataValue(new Variant("tom")));
        String name3 = "Name";
        UaVariableNode node3 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name3))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name3))
                .setDisplayName(LocalizedText.english(name3))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();
        node3.addReference(new Reference(
                node3.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));
        node3.setValue(new DataValue(new Variant("Tom Mustermann")));

        String name4 = "Birthday";
        UaVariableNode node4 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name4))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name4))
                .setDisplayName(LocalizedText.english(name4))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();
        node4.addReference(new Reference(
                node4.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));
        node4.setValue(new DataValue(new Variant("1999-03-02")));
        String name5 = "Password";
        UaVariableNode node5 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name5))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name5))
                .setDisplayName(LocalizedText.english(name5))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();
        node5.addReference(new Reference(
                node5.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));
        node5.setValue(new DataValue(new Variant("123445")));

        String name6 = "Credit";
        UaVariableNode node6 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name6))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name6))
                .setDisplayName(LocalizedText.english(name6))
                .setDataType(Identifiers.Int32)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();
        node6.addReference(new Reference(
                node6.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));
        node6.setValue(new DataValue(new Variant(100)));
        String name7 = "Email";
        UaVariableNode node7 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name7))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name7))
                .setDisplayName(LocalizedText.english(name7))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();
        node7.addReference(new Reference(
                node7.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));
        node7.setValue(new DataValue(new Variant("t.m@ostfalia.de")));


        String name8 = "Usertype";
        UaVariableNode node8 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name8))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name8))
                .setDisplayName(LocalizedText.english(name8))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();
        node8.addReference(new Reference(
                node8.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));
        node8.setValue(new DataValue(new Variant("Recycler")));

        String name9 = "RecGekauft";

        UaVariableNode node9 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name9))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name9))
                .setDisplayName(LocalizedText.english(name9))
                .setDataType(Identifiers.Integer)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .setValueRank(ValueRank.OneDimension.getValue())
                .setArrayDimensions(new UInteger[]{uint(0)})
                .build();
        node9.addReference(new Reference(
                        node9.getNodeId(),
                        Identifiers.HasModellingRule,
                        Identifiers.ModellingRule_Mandatory.expanded(),
                        true
                )
        );

        int[] ids = new int[10];
        Arrays.fill(ids,-1);
        node9.setValue(new DataValue(new Variant(ids)));
//        node9.setAttributeDelegate(new ValueLo);

        Recyclertype.addComponent(node9);
        getNodeManager().addNode(node9);
        Recyclertype.addComponent(node8);
        getNodeManager().addNode(node8);
        Recyclertype.addComponent(node7);
        getNodeManager().addNode(node7);
        Recyclertype.addComponent(node6);
        getNodeManager().addNode(node6);
        Recyclertype.addComponent(node5);
        getNodeManager().addNode(node5);
        Recyclertype.addComponent(node3);
        getNodeManager().addNode(node3);
        Recyclertype.addComponent(node4);
        getNodeManager().addNode(node4);
        Recyclertype.addComponent(node2);
        getNodeManager().addNode(node2);
        Recyclertype.addComponent(node1);
        getNodeManager().addNode(node1);
        try {
            UaObjectNode RecyclerObject = (UaObjectNode) getNodeFactory().createNode(
                    newNodeId("Recycler/Recycler"),
                    Recyclertype.getNodeId(),
                    false
            );


            RecyclerObject.setBrowseName(newQualifiedName("Recycler"));
            RecyclerObject.setDisplayName(LocalizedText.english("Recycler"));


            rootNode.addOrganizes(RecyclerObject);

            RecyclerObject.addReference(new Reference(
                    RecyclerObject.getNodeId(),
                    Identifiers.Organizes,
                    rootNode.getNodeId().expanded(),
                    false
            ));


        } catch (UaException e) {
            logger.error("Error creating MyObjectType instance: {}", e.getMessage(), e);
        }
    }

    private void addObject3(UaFolderNode rootNode) {
        UaObjectTypeNode Ownertype = UaObjectTypeNode.builder(getNodeContext())
                .setNodeId(newNodeId("ObjectTpyes/Ownertype"))
                .setBrowseName(newQualifiedName("Ownertype"))
                .setDisplayName(LocalizedText.english("Ownertype"))
                .setIsAbstract(false)
                .build();
        getNodeManager().addNode(Ownertype);
        getServer().getObjectTypeManager().registerObjectType(
                Ownertype.getNodeId(),
                UaObjectNode.class,
                UaObjectNode::new
        );

        Ownertype.addReference(new Reference(
                Ownertype.getNodeId(),
                Identifiers.HasSubtype,
                Identifiers.BaseObjectType.expanded(),
                false
        ));

        String name = "User_ID";
        UaVariableNode node1 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name))
                .setDisplayName(LocalizedText.english(name))
                .setDataType(Identifiers.Int32)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        node1.addReference(new Reference(
                node1.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));

        node1.setValue(new DataValue(new Variant(0)));
        String name2 = "User_Name";
        UaVariableNode node2 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name2))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name2))
                .setDisplayName(LocalizedText.english(name2))
                .setDataType(Identifiers.Integer)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();

        node2.setValue(new DataValue(new Variant("ye")));
        node2.addReference(new Reference(
                node2.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));
        String name3 = "Name";
        UaVariableNode node3 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name3))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name3))
                .setDisplayName(LocalizedText.english(name3))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();
        node3.addReference(new Reference(
                node3.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));
        node3.setValue(new DataValue(new Variant("Jack Ma")));

        String name4 = "Birthday";
        UaVariableNode node4 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name4))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name4))
                .setDisplayName(LocalizedText.english(name4))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();
        node4.addReference(new Reference(
                node4.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));
        node4.setValue(new DataValue(new Variant("1995-05-13")));

        String name5 = "Password";
        UaVariableNode node5 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name5))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name5))
                .setDisplayName(LocalizedText.english(name5))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();
        node5.addReference(new Reference(
                node5.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));
        node5.setValue(new DataValue(new Variant("123")));

        String name6 = "Credit";
        UaVariableNode node6 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name6))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name6))
                .setDisplayName(LocalizedText.english(name6))
                .setDataType(Identifiers.Int32)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();
        node6.addReference(new Reference(
                node6.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));
        node6.setValue(new DataValue(new Variant(120)));
        String name7 = "Email";
        UaVariableNode node7 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name7))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name7))
                .setDisplayName(LocalizedText.english(name7))
                .setDataType(Identifiers.String)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();
        node7.addReference(new Reference(
                node7.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));
        node7.setValue(new DataValue(new Variant("jackma@alibaba.com")));


        String name8 = "Usertype";
        UaVariableNode node8 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name8))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name8))
                .setDisplayName(LocalizedText.english(name8))
                .setDataType(Identifiers.Integer)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .build();
        node8.addReference(new Reference(
                node8.getNodeId(),
                Identifiers.HasModellingRule,
                Identifiers.ModellingRule_Mandatory.expanded(),
                true
        ));
        node8.setValue(new DataValue(new Variant("Owner")));

        String name9 = "Angebot";
        UaVariableNode node9 = new UaVariableNode.UaVariableNodeBuilder(getNodeContext())
                .setNodeId(newNodeId(name9))
                .setAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setUserAccessLevel(ubyte(AccessLevel.getMask(AccessLevel.READ_WRITE)))
                .setBrowseName(newQualifiedName(name9))
                .setDisplayName(LocalizedText.english(name9))
                .setDataType(Identifiers.Integer)
                .setTypeDefinition(Identifiers.BaseDataVariableType)
                .setValueRank(ValueRank.OneDimension.getValue())
                .setArrayDimensions(new UInteger[]{uint(0)})
                .build();
        node9.addReference(new Reference(
                        node9.getNodeId(),
                        Identifiers.HasModellingRule,
                        Identifiers.ModellingRule_Mandatory.expanded(),
                        true
                )
        );

        int[] vehicles = new int[10];
        Arrays.fill(vehicles,-1);
        vehicles[0]=2;


        node9.setValue(new DataValue(new Variant(vehicles)));

        Ownertype.addComponent(node9);
        getNodeManager().addNode(node9);
        Ownertype.addComponent(node8);
        getNodeManager().addNode(node8);
        Ownertype.addComponent(node7);
        getNodeManager().addNode(node7);
        Ownertype.addComponent(node6);
        getNodeManager().addNode(node6);
        Ownertype.addComponent(node5);
        getNodeManager().addNode(node5);
        Ownertype.addComponent(node3);
        getNodeManager().addNode(node3);
        Ownertype.addComponent(node4);
        getNodeManager().addNode(node4);
        Ownertype.addComponent(node2);
        getNodeManager().addNode(node2);
        Ownertype.addComponent(node1);
        getNodeManager().addNode(node1);
        try {
            getNodeFactory();
            UaObjectNode OwnerObject = (UaObjectNode) getNodeFactory().createNode(
                    newNodeId("Owner/Owner"),
                    Ownertype.getNodeId(),
                    false
            );


            OwnerObject.setBrowseName(newQualifiedName("Owner"));
            OwnerObject.setDisplayName(LocalizedText.english("Owner"));


            rootNode.addOrganizes(OwnerObject);

            OwnerObject.addReference(new Reference(
                    OwnerObject.getNodeId(),
                    Identifiers.Organizes,
                    rootNode.getNodeId().expanded(),
                    false
            ));


        } catch (UaException e) {
            logger.error("Error creating MyObjectType instance: {}", e.getMessage(), e);
        }

    }


    @Override
    public void onDataItemsCreated(final List<DataItem> dataItems) {
        this.subscriptionModel.onDataItemsCreated(dataItems);
    }

    @Override
    public void onDataItemsModified(final List<DataItem> dataItems) {
        this.subscriptionModel.onDataItemsModified(dataItems);
    }

    @Override
    public void onDataItemsDeleted(final List<DataItem> dataItems) {
        this.subscriptionModel.onDataItemsDeleted(dataItems);
    }

    @Override
    public void onMonitoringModeChanged(final List<MonitoredItem> monitoredItems) {
        this.subscriptionModel.onMonitoringModeChanged(monitoredItems);
    }
}
